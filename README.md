# Interface Web PRED

Interface web comprenant deux parties :

    > Affichage de plusieurs scatter 3D représentant les trajectoires d'un utilisateur dans différents mapping
    > Affiche de quelques données propre à un utilisateur

## Pour lancer le projet 

Installation du projet :
```sh
npm install
```
Lancement du projet :

```sh
npm start
```

L'application est maintenant lancé en localhost sur le port 3000
```sh
http://localhost:3000
```
