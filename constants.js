const key_mapping_1 = "0763";
const key_mapping_2 = "3067";
const key_mapping_3 = "6301";
const key_mapping_4 = "0483";
const key_mapping_5 = "7863";
const key_mapping_6 = "7143";

module.exports = Object.freeze({

    key_mapping_1:key_mapping_1,
    key_mapping_2:key_mapping_2,
    key_mapping_3:key_mapping_3,
    key_mapping_4:key_mapping_4,
    key_mapping_5:key_mapping_5,
    key_mapping_6:key_mapping_6,
    API_Base : "https://traces.ls2n.fr/fileservice/",
    //Donnée de test
    CHARLES_INSIDE : '801cafee-aa00-4b7f-be62-fb08ea65ec53',
    CHARLES_OUTSIDE : '18ba4abf-1c7b-4f9a-975b-f1126e64bd1b',
    EMILIEN_INSIDE : 'cee48c15-9b86-4452-bc3f-e7a1f0e407fa',
    EMILIEN_OUTSIDE : '55c70c36-1afb-4dae-ad36-bfaeafc16b03',
    HUGO_D_INSIDE : '1c6088d4-e473-4861-9eda-d2a6ec8b701e',
    HUGO_D_OUTSIDE : 'f70e3d0a-2b20-4c7b-b1eb-9f8fa46672c0',

    LISTE_AXES : [
        'budget', //0
        'company', //1
        'country', //2
        'genre', //3
        'revenue', //4
        'name', //5
        'runtime', //6
        'score', //7
        'year', //8
        'benefit', //9
    ],
    
    

    LISTE_MAPPING : {
        [key_mapping_1] : 
        {
            'x': parseInt(key_mapping_1.split('')[0]), 
            'y': parseInt(key_mapping_1.split('')[1]),
            'z': parseInt(key_mapping_1.split('')[2]),
            'color': parseInt(key_mapping_1.split('')[3]) 
        },
        [key_mapping_2] : 
        {
            'x': parseInt(key_mapping_2.split('')[0]),
            'y': parseInt(key_mapping_2.split('')[1]),
            'z': parseInt(key_mapping_2.split('')[2]),
            'color': parseInt(key_mapping_2.split('')[3]) 
        },
        [key_mapping_3] : 
        {
            'x': parseInt(key_mapping_3.split('')[0]),
            'y': parseInt(key_mapping_3.split('')[1]),
            'z': parseInt(key_mapping_3.split('')[2]),
            'color': parseInt(key_mapping_3.split('')[3]) 
        },
        [key_mapping_4] : 
        {
            'x': parseInt(key_mapping_4.split('')[0]),
            'y': parseInt(key_mapping_4.split('')[1]),
            'z': parseInt(key_mapping_4.split('')[2]),
            'color': parseInt(key_mapping_4.split('')[3]) 
        },
        [key_mapping_5] : 
        {
            'x': parseInt(key_mapping_5.split('')[0]),
            'y': parseInt(key_mapping_5.split('')[1]),
            'z': parseInt(key_mapping_5.split('')[2]),
            'color': parseInt(key_mapping_5.split('')[3]) 
        },
        [key_mapping_6] : 
        {
            'x': parseInt(key_mapping_6.split('')[0]),
            'y': parseInt(key_mapping_6.split('')[1]),
            'z': parseInt(key_mapping_6.split('')[2]),
            'color': parseInt(key_mapping_6.split('')[3]) 
        }
    } 
});