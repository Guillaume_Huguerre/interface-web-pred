var dataviz = null;
var graph = null;
var count = 0;
var window_width = window.innerWidth;
var window_height = window.innerHeight;
var element = document.getElementById("test");
const LISTE_AXES = [
  'budget', //0
  'company', //1
  'country', //2
  'genre', //3
  'revenue', //4
  'name', //5
  'runtime', //6
  'score', //7
  'year', //8
  'benefit', //9
];

const selected_runid = "801cafee-aa00-4b7f-be62-fb08ea65ec53";
const url_getdata_file = '/getdatafromfile'; 
const url_getdata_base = '/getdatafrombase';
const url_mapping_move = '/mapping_move/';

const ID_Mapping = ["mapping_1_graph"];

const slider_size_default = 70;

var mode_env; //1: Inside - 2: Outside
var liste_deplacement = [];
var slider_bool_first = [];
var trace2 = {
	x:[0], y: [0], z: [1.6],
	mode: 'markers',
	marker: {
		color: 'rgb(127, 127, 127)',
		size: 30,
		symbol: 'square',
	
		},
	type: 'scatter3d'

};
  
//Event pour affichager les graphs
$("#test").on('click', () => {
  load_mapping_move(selected_runid);
});

//Event pour afficher les données des runs
$('#test2').on('click', () => {
  load_data_(selected_runid,'HMD');
});


//Fonction du choix de l'environnement
function choose_env(runid) {
  
  $.get("https://traces.ls2n.fr/fileservice/trials/INOUTCUBE2021/", function (data) {   
    
    let current_env;
    for(key in data.runs) {
      
      (data.runs[key]).forEach(element => {
        
        if (element.runid == runid) {
          
          current_env = element.environmentid;
        }
      });
    }
    for(key in data.envs) {
      
      (data.envs[key]).forEach(element => {
        
        if (element.environmentid == current_env) {
          if (element.value == 'CubeInside') {
            mode_env = 1;
          } 
          else {
            mode_env = 2;
          }
        }
      }); 
    }

  });
}

//Fonction initiale
function goto_mapping_move(html){   
  $('#body').html(html);  
  $(".arrow").on('click',(elem) => {
    let current_id = elem.target.id;
    current_id = current_id.split("_")[(current_id.split("_")).length - 1];
    let current_class = elem.target.classList[0];    
    let incr;
    switch (current_class) {
      case 'img_fleche_haut_2sec':
        incr = 2;
        break;
      case 'img_fleche_bas_2sec':
        incr = -2;
        break;
      case 'img_fleche_haut_10sec':
        incr = 10;
        break;
      case 'img_fleche_bas_10sec':
        incr = -10;
        break;
    }    
    $('#myRange_'+current_id)[0].value = parseInt($('#myRange_'+current_id)[0].value) + parseInt(incr);
    update_deplacement(current_id);
  });
}

//Fonction de convertion du temps
function build_time(time) {   
  
  var hrs = ~~(time / 3600);
  var mins = ~~((time % 3600) / 60);
  var secs = ~~time % 60;

  
  var ret = "";
  if (hrs > 0) {
      ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
  }
  ret += "" + mins + ":" + (secs < 10 ? "0" : "");
  ret += "" + secs;
  return ret;
}

//Fonction de base
function load_mapping_move(runid) {
  choose_env(runid);
  $.get(url_mapping_move+runid, function (data) {    
    let count = 1;  
    let html = "";
    let max_duration = 0;
    let nb_mapping = 0;
    let start_time = 0;
    let first_mapping = true;
    data.forEach(element => {
      for (key in element) {   
        if (first_mapping) {
          start_time = element[key].start;
          first_mapping = false;
        }
        if ((element[key].liste_data).length >= 200) {          
          if (element[key].end - element[key].start > max_duration) {
            max_duration = element[key].end - element[key].start;
          }
          html += build_html(count,(element[key].end - element[key].start),build_time(element[key].start - start_time),build_time(element[key].end - start_time))
          count++;
        }
      }
    });
    nb_mapping = count - 1;
    goto_mapping_move(html);
    count_bis = 1;
    
    data.forEach(element => {
              
        let data;
        let insight;
        let deplacement;
        for(key in element) {
          if ((element[key].liste_data).length >= 200) {          
            data = element[key].liste_data;
            insight = element[key].liste_insight;
            deplacement = element[key].liste_point_deplacement;           
            let data_move = buildscatter3D_line(data);            
            liste_deplacement.push(deplacement);
            let duration = element[key].end - element[key].start;
            let slider_size = slider_size_default*(duration/max_duration);
            set_slider_size(count_bis,slider_size);
            
            let liste_start_insights = [];
            let liste_end_insights = [];
            insight.forEach(element_insight => {
              if (element_insight.verb == "start") {
                liste_start_insights.push(
                  {
                    x:element_insight.x,
                    y:element_insight.y,
                    z:element_insight.z,
                  }
                )
              }
              else {
                liste_end_insights.push(
                  {
                    x:element_insight.x,
                    y:element_insight.y,
                    z:element_insight.z,
                  }
                )
              }              
            });
            
            data_start_insights = buildscatter3D_markers(liste_start_insights,15);
            data_end_insights = buildscatter3D_markers(liste_end_insights,5);
            let current_mapping_name = 'Mapping : ' + LISTE_AXES[key.split("")[0]]+ ' x ' + LISTE_AXES[key.split("")[1]]+ ' x ' + LISTE_AXES[key.split("")[2]]+' | color : ' + LISTE_AXES[key.split("")[3]]+ '<br> Nombre d\'insights trouvé : ' + liste_start_insights.length + '<br> Temps resté dans le mapping : '+duration +' secondes';
            var layout1 = {
              title: current_mapping_name,
              scene:{
                aspectmode:'cube',
                xaxis: {              
                  range: [-4.5, 5.5]
                },
                yaxis: {                  
                  range: [-5, 5]                                    
                },
                zaxis: {                  
                  range: [-4.5, 5.5]                                
                }
              }
            };
            var layout2 = {
              title: current_mapping_name,
              scene:{
                aspectmode:'cube',
                xaxis: {                                   
                  range: [-1, 1]
                },
                yaxis: {                                    
                  range: [-1, 1]
                },
                zaxis: {
                  range: [1.3, 2]
                }
              }
            };
            let current_layout;
            if (mode_env == 1) {
              current_layout = layout1;
            }
            else {
              current_layout = layout2;
            }
            draw_graph("mapping_"+count_bis+"_graph",[data_move,data_start_insights,data_end_insights],current_layout);  
            addMarker(count_bis,element[key].liste_data[0],'Début de la trajectoire','blue',10);
            addMarker(count_bis,element[key].liste_data[(element[key].liste_data.length) - 1],'Fin de la trajectoire','black',10);
            count_bis ++;
            slider_bool_first.push(true);
          }
        }      
    });    
  });   
}

function unpack(rows, key) {
  return rows.map(function(row) 
  { return row[key]; }); 
}

//Fonction de création d'un graph
function draw_graph(id_graph, data,layout)
{
    Plotly.newPlot(id_graph, data,layout);
}

//Fonction de création d'un tracé
function buildscatter3D_line(data) {
  let x = (unpack(data,'x'));
  let y = (unpack(data,'y'));
  let z = (unpack(data,'z'));
  let color = (unpack(data,'color'));
  let build_data = {
    type: 'scatter3d',
    mode: 'lines',      
    x: x,
    y: y,
    z: z,
    name: 'Trajectoire',
    opacity: 1,
    line: {
      width: 1,
      color: color,
      reversescale: false
    }
  }
  return build_data;
}

//Fonction de création d'un marker en spécifiant une taille
function buildscatter3D_markers(data,size) {
  let x = (unpack(data,'x'));
  let y = (unpack(data,'y'));
  let z = (unpack(data,'z'));  
  let build_data = {
    type: 'scatter3d',
    mode: 'markers',
    x: x,
    y: y,
    z: z,
    name: 'Insights',
    opacity: 1,
    marker: {
      color: 'red',
      size: size,
      symbol: 'circle',
      line: {
        color: 'red',
        width: 1
      },
      opacity: 0.8
    },
  }
  return build_data;
}

//Template html
function build_html(id,time,start_time,end_time) {
  let _time = time-1;
  let template = "<div id='mapping_"+id+"' class='content'>";
  template += "<div class='fleche_graph'>";
  template += "<div class='fleche'>";

  template += "<img id='arrow_top_2sec_"+id+"' class='img_fleche_haut_2sec arrow' src='./arrow.svg'>";

  template += "<div class='text_fleche_haut_2sec'>+ 2sec</div>";
  template += "<div class='text_fleche_bas_2sec'>- 2sec</div>";

  template += "<img id='arrow_bot_2sec_"+id+"' class='img_fleche_bas_2sec arrow' src='./arrow.svg'>            ";


  template += "<img id='arrow_top_10sec_"+id+"' class='img_fleche_haut_10sec arrow' src='./arrow.svg'>";

  template += "<div class='text_fleche_haut_10sec'>+ 10sec</div>";
  template += "<div class='text_fleche_bas_10sec'>- 10sec</div>";

  template += "<img id='arrow_bot_10sec_"+id+"' class='img_fleche_bas_10sec arrow' src='./arrow.svg'>";

  template += "</div>";

  template += "<div class='mapping_graph' id='mapping_"+id+"_graph'></div>";
  
  template += "<div class='slidecontainer'>";
  template += "<p class='text_slider test_start'>"+start_time+"</p>"
  template += "<input type='range' min='0' max='"+_time+"' step='1' value='0' class='slider' id='myRange_"+id+"' oninput=\"update_deplacement(String(this.id).split('_')[1])\" >";
  template += "<p class='text_slider test_end'>"+end_time+"</p>"
  template += "<p id='current_time_"+id+"' class='current_time' ></p>"
  template += "</div>";
  template += "</div>";
  template += "</div>";

  return template;
}

//Fonction changeant la taille du slider
function set_slider_size(id,taille) {
  $('#myRange_'+id).css('width',taille+'%');
}

//Fonction permettant d'update la position du marker de position de l'utilisateur
function update_deplacement(id) {

  let current_start_time = $('#mapping_'+id+' .fleche_graph .slidecontainer .test_start').html().split(':');
  let current_start_min = parseInt(current_start_time[0]);
  let current_start_sec = parseInt(current_start_time[1]);
  let current_start_time_sec = current_start_min*60 + current_start_sec;

  let value = parseInt($('#myRange_'+id)[0].value);
  let current_time = current_start_time_sec+(value);
  $("#current_time_"+id).html(build_time(current_time));
  let x = liste_deplacement[id - 1][value].x;
  let y = liste_deplacement[id - 1][value].y;
  let z = liste_deplacement[id - 1][value].z;
  if (slider_bool_first[id - 1]){
    
    addMarker(id,liste_deplacement[id - 1][value],'Position de l\'utilisateur','green',10);
    slider_bool_first[id - 1] = false;
  }
  deleteMarker(id);  
  addMarker(id,liste_deplacement[id - 1][value],'Position de l\'utilisateur','green',10);
}

//Fonction générique d'ajout d'un marker
function addMarker(id,data,name,color,size) {

  var mark =
  {
    type: 'scatter3d',
    mode: 'markers',      
    x: [data.x],
    y: [data.y],
    z: [data.z],
    name: name,
    opacity: 1,
    marker: {
      color: color,
      size: size,
      symbol: 'circle',
      line: {
      color: 'red',
      width: 1},
      opacity: 0.8},
  }
  Plotly.addTraces("mapping_"+id+"_graph", mark);
}

//Fonction supprimant le dernier marker ajouté sur une objet plotly
function deleteMarker(id) {
  Plotly.deleteTraces("mapping_"+id+"_graph", -1);
}


//Fonction d'affichage des données "basique"
function load_data_(runid,trackertype) {
  
  const promise1 = new Promise((resolve, reject) => {
    let url = "getnbdataselect/";
    $.get(url+runid, (data) => {                  
      resolve(data);
    })
  });
  const promise2 = new Promise((resolve, reject) => {
    let url = "/getdistance/";
    $.get(url+runid+"/"+trackertype, (data) => {
      resolve(data);
    })
  });
  const promise3 = new Promise((resolve, reject) => {
    let url = "/gettimemappinginsight/";
    $.get(url+runid, (data) => {            
      resolve(data);
    })
  });
  Promise.all([promise1,promise2,promise3]).then((values) => {
    let html_bis = "<table>";
    html_bis += "<thead>";
    html_bis += "    <tr>";
    html_bis += "        <th>Distance_parcourue</th>";
    html_bis += "        <th>Nombre de sélection de données</th>";
    html_bis += "        <th colspan='6'>Temps pour trouvez un insight</th>";
    html_bis += "    </tr>";
    html_bis += "</thead>";
    html_bis += "<tbody>";
    html_bis += "    <tr>";
    html_bis += "        <td>"+parseInt(values[1])+"</td>";
    html_bis += "        <td>"+parseInt(values[0])+"</td>";

    html_bis += "        <td>"+parseInt(values[2][0])+"</td>";
    html_bis += "        <td>"+parseInt(values[2][1])+"</td>";
    html_bis += "        <td>"+parseInt(values[2][2])+"</td>";
    html_bis += "        <td>"+parseInt(values[2][3])+"</td>";
    html_bis += "        <td>"+parseInt(values[2][4])+"</td>";
    html_bis += "        <td>"+parseInt(values[2][5])+"</td>";
    html_bis += "    </tr>";

    html_bis += "</tbody>";
    html_bis += "</table>";

    $('#body').html(html_bis);
  });
  


}