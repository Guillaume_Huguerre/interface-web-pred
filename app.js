const https = require('https');
var constants = require('./constants');
const async = require('async');
const request = require('request');

module.exports = function(app){    

    //route pour générer les données de déplacement par mapping
    app.get ('/mapping_move/:runid',(req,res)=>{
        var list_map = [];                
        var return_table = [];
        var data_events;
        var data_events_mapping;
        var result_events = [];
        var data_insights = [];
        var data_trackers;
        var start_expe;
        var end_expe;        
        const urls= [
            constants.API_Base+'events/'+req.params.runid+'/',
            constants.API_Base+'trackers/'+req.params.runid+'/HMD'                      
          ];
        function httpGet(url, callback) {
          const options = {
            url :  url,
            json : true
          };
          request(options,
            function(err, resu, body) {
              callback(err, body);
            }
          );
        }
                   
        async.map(urls, httpGet, function (err, resu){
            
          if (err) return console.log(err);                    
            data_events_mapping = resu[0].properties;
            data_events = resu[0].events;
            data_trackers = resu[1].HMD;

            data_trackers.sort(function(a, b){
              return a.apptime - b.apptime;
            });
            data_events.sort(function(a, b) {
                return a.apptime - b.apptime;
            });        
            
            
            data_events.forEach(element => {
                if (element.object == 'change mapping') {                    
                  result_events.push(
                    {
                      "event_id":element.id,
                      "apptime":element.apptime
                    }
                  )
                }
                else if (element.object == 'comment record') {
                    data_insights.push(
                        {
                            'apptime':element.apptime,
                            'verb':element.verb
                        }
                    )
                }
                else if (element.object == 'expe') {
                    if (element.verb == 'start') {
                        start_expe = element.apptime;
                    }
                    else {
                        end_expe = element.apptime;
                    }
                }              
            });
            list_map.push(
                {
                    'id_mapping': constants.key_mapping_1,
                    'apptime': start_expe
                }
            );
            return_table.push({
                [constants.key_mapping_1]: {
                    "liste_data" : [

                    ],
                    "liste_insight": [
                
                    ],
                    "liste_point_deplacement": [
                      
                    ],
                    "start": parseInt(start_expe),
                    "end": 0
                }
            });
            result_events.forEach(element => {
                var xAxis = data_events_mapping[element.event_id][0].value;
                var yAxis = data_events_mapping[element.event_id][1].value;
                var zAxis = data_events_mapping[element.event_id][2].value;
                var color = data_events_mapping[element.event_id][3].value;
                list_map.push(
                    {
                        'id_mapping': xAxis+yAxis+zAxis+color,
                        'apptime': element.apptime
                    }
                )
                return_table.push({
                    [xAxis+yAxis+zAxis+color]: {
                        "liste_data" : [

                        ],
                        "liste_insight": [
                    
                        ],
                        "liste_point_deplacement": [
                          
                        ],
                        "start": parseInt(element.apptime),
                        "end": 0
                    }
                })
            });
            
            let count = 0;                    
            list_map.forEach(element => {
                if (count < list_map.length-1) {
                
                   return_table[count][list_map[count].id_mapping].end = parseInt(return_table[count+1][list_map[count+1].id_mapping].start);      
                }
                else {                    
                    return_table[count][list_map[count].id_mapping].end = parseInt(end_expe);
                }
                count ++;
            });
                        
            let count_list_map = 0;
            let count_insight = 0; 
            let count_deplacement = 0;      
                                        
            data_trackers.forEach(element => {  
                
                if (count_insight < data_insights.length){
                    if(parseInt(element.apptime) == parseInt(data_insights[count_insight].apptime)) {
                        (return_table[count_list_map][list_map[count_list_map].id_mapping].liste_insight).push(
                            {
                                'x': element.posX,
                                //'y': element.posY,
                                //'z': element.posZ,
                                'y': element.posZ,
                                'z': element.posY,
                                'verb': data_insights[count_insight].verb 
                            }
                        )
                        count_insight++;
                    } 
                }
                
                if (count_list_map > list_map.length-2) {
                    
                    (return_table[count_list_map][list_map[count_list_map].id_mapping].liste_data).push(
                        {
                            'x': element.posX,                                                  
                            'y': element.posZ,
                            'z': element.posY
                        }
                    )
                   
                    count_deplacement++;                    
                }
                else if(element.apptime > list_map[count_list_map].apptime && element.apptime < list_map[count_list_map+1].apptime) {                                    
                    
                    (return_table[count_list_map][list_map[count_list_map].id_mapping].liste_data).push(
                        {
                            'x': element.posX,
                            'y': element.posZ,
                            'z': element.posY
                        }
                    )
                   
                    count_deplacement ++;
                } else {
                    count_list_map++;
                    count_deplacement = 0;
                }            
            });    
            
            return_table.forEach(element => {
                for (key in element) {                                   
                    let count = 0;                   
                    (element[key].liste_data).forEach(elementbis => {
                        if (count % parseInt(((element[key].liste_data).length/(element[key].end - element[key].start))) == 0) {
                            element[key].liste_point_deplacement.push(
                                {                                    
                                    'x': elementbis.x,
                                    'y': elementbis.y,
                                    'z': elementbis.z                                   
                                }
                            )
                        }
                        count++;
                    });
                }
            });
            res.send(return_table);
        });
    });
    //Route pour récuperer le temps pour trouver un insight dans un mapping
    app.get('/gettimemappinginsight/:runid', (req, res) => {
        let runid = req.params.runid;
        const url = constants.API_Base+'events/'+runid+'/';   
        let time_mapping = [];
        https.get(url, (resp) => {
            let data = '';
            let result = [];
            resp.on('data', (chunk) => {
                data += chunk;     
            });

            resp.on('end', () => {     
                data = JSON.parse(data);                
                data = data.events;
                let count = 0;
                data.sort(function(a, b){
                    return a.apptime - b.apptime;
                });
                    
                data.forEach(element => {
                    if (element.verb == 'launch' && element.object == "change mapping") {
                    result.push(
                        {
                        "apptime":element.apptime,
                        "verb":element.verb
                        }
                    )
                    }
                    if (element.verb == 'start' && element.object == "comment record") {
                    result.push(
                        {
                        "apptime":element.apptime,
                        "verb":element.verb
                        }
                    )
                    }
                    if (element.verb == 'end' && element.object == "comment record") {
                    result.push(
                        {
                        "apptime":element.apptime,
                        "verb":element.verb
                        }
                    )
                    }
                    count ++;
                });                
                count = 0;      
                result.forEach(element => {        
                    if (count + 2 < result.length) {
                    
                    if (element.verb == 'launch') {
                        for(let i = count+1; result[i].verb != 'launch' ; i++) {            
                        
                        if(result[i].verb == 'start' && result[i+1].verb == 'end' ){              
                            time_mapping.push(result[i].apptime - element.apptime);
                            break;
                        }              
                        }
                    } 
                    }
                    count ++;
                });      
                
                res.send(time_mapping);
            });
        }).on("error", (err) => {
            console.log("Error: " + err.message);
        });
        
    });

    //Route pour obtenir la distance parcourue dans un environnement
    app.get('/getdistance/:runid/:trackertype', (req, res) => { 
        let runid = req.params.runid;
        let trackertype = req.params.trackertype;
        const url = constants.API_Base+'distances/'+runid+'/'+trackertype;   
        https.get(url, (resp) => {
          let data = '';
          let result = 0;
          resp.on('data', (chunk) => {
            data += chunk;     
          });
      
          resp.on('end', () => {              
            (JSON.parse(data)).forEach(element => {        
              result += element.distance;        
            });                     
            res.send(String(result));
          });
          
        }).on("error", (err) => {
          console.log("Error: " + err.message);
        });
    });

    //Route pour obtenir le nombre de données sélectionné dans un run
    app.get('/getnbdataselect/:runid', (req, res) => { 
        let runid = req.params.runid;
        const url = constants.API_Base+'events/'+runid+'/';   
        
        https.get(url, (resp) => {
            let data = '';
            let result = [];
            var already_selected = false;
            var current_object = 0;
            var current_apptime = 0;
            const time_new_object = 1;

            var count_selected_data = 0;
            resp.on('data', (chunk) => {
                data += chunk;     
            });

            resp.on('end', () => {     
                data = JSON.parse(data);      
                data = data.events;
                data.sort(function(a, b){
                    return a.apptime - b.apptime;
            });
                
            data.forEach(element => {
                if (element.verb == 'focus') {
                result.push(
                    {
                    "apptime":element.apptime,              
                    "object":element.object              
                    }
                )
                }              
            });  
            
            result.forEach(element => {        
                if(element.object != current_object) {
                already_selected = false;
                current_object = element.object;
                current_apptime = element.apptime;
                }
                if ((current_apptime + time_new_object) <= element.apptime && !already_selected) {
                already_selected = true;
                count_selected_data++;
                }
            
            });      
            res.send(String(count_selected_data));
            });
        }).on("error", (err) => {
            console.log("Error: " + err.message);
        });
    });      
}